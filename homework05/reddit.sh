#!/bin/sh

nopt=10

while getopts ":hrsn:" opt; do
  case $opt in
    h) hopt=$opt;;
    r) aopt=$opt;;
    s) aopt=$opt;;
    n) nopt=$OPTARG;;
    *) echo "Invalid argument"
        exit 1
  esac
done

if [ ! -z $hopt ]; then
  echo "usage: reddit.sh [options] subreddits...

      -r      Shuffle the links
      -s      Sort the links
      -n N    Number of links to display (default is 10)"

  exit 0
fi

for arguments in $@; do
  if [ ! -z $aopt ]; then
    if [ $aopt = '-r' ]; then
      curl -s http://www.reddit.com/r/$arguments/.json 2> /dev/null | python -m json.tool 2> /dev/null | grep url | grep -oe "http.*" | grep -v redditmedia | sed 's/\",//g' | sort -R | head -n $nopt
    
    elif [ $aopt = '-s' ]; then
      curl -s http://www.reddit.com/r/$arguments/.json 2> /dev/null | python -m json.tool 2> /dev/null | grep url | grep -oe "http.*" | grep -v redditmedia | sed 's/\",//g' | sort | head -n $nopt
    fi
 
  else
    curl -s http://www.reddit.com/r/$arguments/.json 2> /dev/null | python -m json.tool 2> /dev/null | grep url | grep -oe "http.*" | grep -v redditmedia | sed 's/\",//g' | head -n $nopt
    
  fi
done
