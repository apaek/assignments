Homework 05
===========

Activity 1
----------

1. The source set consists of the 26 letters of the alphabet. Therefore, I created a variable holding the alphabet, repeated twice, and made SET1 only the first 26 characters by starting the at the 0th letter and ending at 26 characters after the 0th letter.

2. The target set consists of the starting letter as the letter of place $argument. For example, if $argument is 13, the the starting character must be the 13th letter. The ending letter must be 26 letters after the $argument, looping around the alphabet. By using `${alph:${argument}:26}`, it will start at the $argument's letter and end at 26 letters after $argument.

3. By using `tr`, I transformed from the source set to the target set. By using `${alph:${argument}:26}`, the target set starts with the letter at the $argument's spot and continues for 26 letters. Therefore, the char 'a' would be mapped to the char 'n' if $argument was 13. 

Activity 2
----------

1. I filtered the URLs and only extracted the relevant portions by using grep. I first filered for only lines containing the string "url", then grepped the portion of hte output that followed from "http". I then removed any extra characters at the end of the url.

2. I managed the different orderings with an if statement. It checked the value of the argument, and used a different command depending on its value. For example, in the shuffle (-r), I included a sort -R for it to randomly shuffle the URLs and then outputted only the number necessary. If the argument wasnt defined (meaning neither -r nor -s was included), it would just output without any special sorting so it could output as is.

3. Because of the command line options, I had to include getopts so that I can find the flags used. I also had to have multiple if statements to determine which commands I had to invoke.

Activity 3
----------

1. I removed comments by searching for the comment delimiter and deleting any characters including and after the delimiter using sed. Because sed requires you to escape special characters, I included a forloop that edited the value of the delimiter including the backspace character before each character in the delimiter so that sed can work properly.

2. I removed empty lines by using sed to search for any lines that had no characters in them. Any lines with trailing whitespaces had those whitespaces deleted. Therefore, all whitespace-only lines were turned to empty lines before the empty line removal. Also, comments were also deleted before the trailing whitespaces were deleted so that comment lines would also be deleted.

3. I had to use getopts to determine which flags were used. I then had to have if statents to determine which options were chosen and execute the correct command. 
