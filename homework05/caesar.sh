#!/bin/sh

if [ ! -z $1 ]; then
  argument=$1
else
  argument=13
fi

#usage
if [ $argument = "-h" ]; then
  echo "usage: caesar.sh [rotation]

  This program will read from stdin and rotate (shift right) the text by
  the specified rotation.  If none is specified, then the default value is
  13."
  exit 0
fi

#change argument to be <26

if [ $argument -gt 26 ]; then
  argument=$((argument%26))
fi
#get stdin
#set the string set for tr
alph=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
ALPH=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ


#use tr to translate the input value's lowercase and uppercase characters
cat | tr "${alph:0:26}" "${alph:${argument}:26}" | tr "${ALPH:0:26}" "${ALPH:${argument}:26}"
