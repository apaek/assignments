#!/bin/sh
dopt='#'
while getopts ":hWd:" opt; do
  case $opt in
    h) hopt=$opt;;
    W) wopt=$opt;;
    d) dopt=$OPTARG;;
    *) echo "Invalid argument"
        exit 1
  esac
done

if [ ! -z $hopt ]; then
  echo "usage: broify.sh

    -d DELIM    Use this as the comment delimiter.
    -W          Don't strip empty lines."
  exit 0
fi
back='\'
for ((i=0;i<${#dopt};i++)); do
  holder="$holder$back${dopt:$i:1}"
done
dopt=$holder
if [ ! -z $wopt ]; then
  cat | sed "s/$dopt.*//" | sed 's/[ \t]*$//'
else
  cat | sed "s/$dopt.*//" | sed 's/[ \t]*$//' | sed '/^$/d'
fi
