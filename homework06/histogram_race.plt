set terminal png
set output "race.png"
set grid
set xrange [2012:2019]
set yrange [0:100]
set style data histogram
set style fill solid border
set boxwidth 0.1
set xtics 2012,1,2019
plot 'race.dat' using ($1-.33):2 title "Caucasian" with boxes,\
     'race.dat' using ($1-.22):3 title "Oriental" with boxes,\
     'race.dat' using ($1-.11):4 title "Hispanic" with boxes,\
     'race.dat' using ($1+.00):5 title "African American" with boxes,\
     'race.dat' using ($1+.11):6 title "Native American" with boxes,\
     'race.dat' using ($1+.22):7 title "Mutliple Selection" with boxes,\
     'race.dat' using ($1+.33):8 title "Undeclared" with boxes
exit
