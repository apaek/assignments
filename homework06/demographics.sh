#!/bin/sh
file='demographics.csv'
#for line in $(cat demographics.csv); do
  cat $file | awk -F "," '{
      st13[$1]++;
      st13[$2]++;
      st14[$3]++;
      st14[$4]++;
      st15[$5]++;
      st15[$6]++;
      st16[$7]++;
      st16[$8]++;
      st17[$9]++;
      st17[$10]++;
      st18[$11]++;
      st18[$12]++;
    } 
    END {
      printf("Year    C    O    S    B    N    T    U\n");
      printf("2013%5d%5d%5d%5d%5d%5d%5d\n",st13["C"],st13["O"],st13["S"],st13["B"],st13["N"],st13["T"],st13["U"]);
      printf("2014%5d%5d%5d%5d%5d%5d%5d\n",st14["C"],st14["O"],st14["S"],st14["B"],st14["N"],st14["T"],st14["U"]);
      printf("2015%5d%5d%5d%5d%5d%5d%5d\n",st15["C"],st15["O"],st15["S"],st15["B"],st15["N"],st15["T"],st15["U"]);
      printf("2016%5d%5d%5d%5d%5d%5d%5d\n",st16["C"],st16["O"],st16["S"],st16["B"],st16["N"],st16["T"],st16["U"]);
      printf("2017%5d%5d%5d%5d%5d%5d%5d\n",st17["C"],st17["O"],st17["S"],st17["B"],st17["N"],st17["T"],st17["U"]);
      printf("2018%5d%5d%5d%5d%5d%5d%5d\n",st18["C"],st18["O"],st18["S"],st18["B"],st18["N"],st18["T"],st18["U"]);

    }' > race.dat
    
  cat $file | awk -F "," '{
      st13[$1]++;
      st13[$2]++;
      st14[$3]++;
      st14[$4]++;
      st15[$5]++;
      st15[$6]++;
      st16[$7]++;
      st16[$8]++;
      st17[$9]++;
      st17[$10]++;
      st18[$11]++;
      st18[$12]++;
    } 
    END {
      printf("Year    M    F\n");
      printf("2013%5d%5d\n", st13["M"], st13["F"]);
      printf("2014%5d%5d\n", st14["M"], st14["F"]);
      printf("2015%5d%5d\n", st15["M"], st15["F"]);
      printf("2016%5d%5d\n", st16["M"], st16["F"]);
      printf("2017%5d%5d\n", st17["M"], st17["F"]);
      printf("2018%5d%5d\n", st18["M"], st18["F"]);

    }' > gender.dat
#done
