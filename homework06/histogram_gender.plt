set terminal png
set output "gender.png"
set grid
set xrange [2012:2019]
set yrange [0:100]
set style data histogram
set style fill solid border
set boxwidth 0.40
set xtics 2012,1,2019
plot 'gender.dat' using ($1-.21):2 title "males" with boxes,\
     'gender.dat' using ($1+.21):3 title "females" with boxes
exit
