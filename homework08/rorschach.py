#!/usr/bin/env python2.7

import sys, getopt, os, re, fnmatch, yaml, time


VERBOSE=False
TIME = 2
RULES = '.rorschach.yml'
DIRECTORIES= ['.']
runs=0


try:
  opts, args = getopt.getopt(sys.argv[1:], "hr:t:v")
except getopt.GetoptError:
  print 'Invalid option. Use -h for help'
  sys.exit(1)

for opt, arg in opts:
  if opt=='-h':
    print '''Usage: rorschach.py [-r RULES -t SECONDS] DIRECTORIES...

    Options:

        -r RULES    Path to rules file (default is .rorschach.yml)
        -t SECONDS  Time between scans (default is 2 seconds)
        -v          Display verbose debugging output
        -h          Show this help message'''
    sys.exit(0)
  elif opt == '-r':
    RULES = arg
  elif opt == '-t':
    TIME = int(arg)
  elif opt == '-v':
    VERBOSE = True;

def debug(message, *debugargs):
  if VERBOSE:
    print message.format(*debugargs)

with open(RULES, 'r') as stream:
  debug('Loading rules from {}', RULES)
  try:
    rules= yaml.load(stream)
  except yaml.YAMLError as e:
    print(e)
pattern =[]
action =[]
action_file=[]
regex = []
for i in range(len(rules)):
  d=rules[i]
  
  try:
    pattern.append(re.compile(d['pattern']))
    regex.append(True)
  except:
    pattern.append(d['pattern'])
    regex.append(False)
  action.append( d['action'])
  action_file.append(action[i].split(' '))
  debug('setting pattern as "{}" and action as "{}"', pattern, action) 
 
if args:
  DIRECTORIES = args[0:]
filelist={}

def check_directory():
  for i in range(len(DIRECTORIES)):
    for dirpath, dirnames, filnames in os.walk(DIRECTORIES[i]):
      for filename in filnames:
        if filename in filelist:
          file_mode = os.stat(dirpath+'/'+filename)
          if filelist[filename] < file_mode.st_mtime:
            filelist[filename] = file_mode.st_mtime
            if check_file(dirpath+'/'+filename):
              execute_action(dirpath+'/'+filename)
        else:
          file_mode = os.stat(dirpath+'/'+filename)
          filelist[filename] = file_mode.st_mtime
          check_file(dirpath+'/'+filename)
def check_file(filename):
  debug('matchig {} to pattern', filename)
  for i in range(len(rules)):
    if regex[i]:
      if pattern[i].match(filename):
        execute_action(filename, i)
    else:
      if fnmatch.fnmatch(filename,pattern[i]):
        execute_action(filename,i)

  return False

def execute_action(filename, i):
  childpid = os.fork()
  if childpid == 0:
    for j in range(len(action_file[i])):
      if action_file[i][j] == '{path}':
        action_file[i][j] = filename
      elif action_file[i][j] == '{name}':
        action_file[i][j] = os.path.basename(filename)
    os.execvp(action_file[i][0], action_file[i])
    os._exit(0)
  else:
    pid, status = os.wait()
while True:
  time.sleep(TIME)
  os.system('clear')
  check_directory()

  

