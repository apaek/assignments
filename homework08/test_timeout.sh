#!/bin/sh

file='./timeout.py'

if [ -x $file ]; then
  echo 'timeout.py is executable'
else
  echo 'timeout.py is not executable'
  exit 1
fi

if ( cat $file | grep /bin ); then
  echo 'has she-bang'
else
  echo 'does not have she-bang'
  exit 1
fi

if ( $file -h > /dev/null 2>&1); then
  echo "$file prints helpful message"
else
  echo "$file does not print a helpful message with the flag '-h'"
  exit 1
  
fi


for N in $(seq 4); do
  if ( $file -t 5 sleep $N ); then
    echo "Success when N = $N"
  else
    echo "$file fails when sleeping for $N seconds when the timeout is 5 seconds"
    exit 1
  fi
done

for N in $(seq 2 5); do
  if ( $file -t 1 sleep $N ); then
    echo "failure!, $file succeeded with a time out of 1 second and a sleep of $N seconds"
    exit 1
  else
    echo "success, timeout fails sleep for $N seconds"
  fi
done

if ( $file -v -t 2 sleep 1 > /dev/null 2>&1); then
  echo "Success! $file prints out extra information"
else
  echo "failure, $file does not print out extra info with the -v flag"
  exit 1
fi

echo 'timeout is successful!'
