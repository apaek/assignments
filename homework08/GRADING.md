Homework 08 - Grading
=====================

**Score**: 14 / 15

Deductions
----------

### rorschach.py

* -1.00 **--** Prints out names of unchanged files at very beginning when it shouldnt, prints out pathname incorrectly, and errors (throws exception) on exit.

Comments
--------
