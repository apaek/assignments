#!/usr/bin/env python2.7

import sys,getopt,os,signal,errno

optT=0
optV=0
try:
  opts, args = getopt.getopt(sys.argv[1:], "ht:v")
except getopt.GetoptError:
  print 'Invalid option. Use -h for help'
  sys.exit(1)

for opt, arg in opts:
  if opt =='-h':
    print '''Usage: timeout.py [-t SECONDS] command...

    Options:

      -t SECONDS  Timeout duration before killing command (default is 10 seconds)
      -v          Display verbose debugging output'''
    print >>sys.stderr, 'help was called'
    sys.exit(0)
  elif opt=='-t':
    optT = int(arg)
  elif opt == '-v':
    optV = 1

def debug(message, *debugargs):
  #print message formatted with args to sys.stderr if verbose is true
  if optV:
    print >>sys.stderr, 'verbose'
    print message.format(*debugargs)

def handler(signum, frame):
  debug('Alarm Triggered after {} seconds!', optT)
  debug('Killing PID {}',childpid);
  os.kill(childpid, signal.SIGTERM);

def child(args):
  try:
    os.execvp(args[0], args)
  except:
    commands = ' '.join(str(x) for x in args) 
    print "Error, command {} could not run".format(commands)
  os._exit(0)
  
command = ' '.join(str(x) for x in args)
debug('Executing "{}" for at most {} seconds...', command, optT)

debug('Forking...')
childpid = os.fork()
if childpid == 0:
  debug('Execing...')
  child(args)
else:
  debug('Enabling Alarm...')
  signal.signal(signal.SIGALRM, handler)
  signal.alarm(optT)
  try:
    debug('Waiting...')
    pid, status = os.wait()
  except OSError as e:
    if e.errno == errno.EINTR:
      pid, status = os.wait()
  debug('Disabling alarm...')
  signal.alarm(0)

debug('Process {} terminated with exit status {}', pid, status)
sys.exit(status)
