Homework 08
===========

Activity 01
-----------

1. The parent process took on the role of waiting for the child process. It had an alarm that would kill the child once time was reached if the child did not exit on its own. The child had the role of executing the command specified by the user. The parent used os.wait() to wait for the child and signal.alarm to kill it if it did not exit in time. The child used os.execvp to execute the command that was specified.

2. The timeout mechanism sets an alarm in the parent process. If the child does not exit by the time the alarm goes off, the alarm kills the child, using the child's pid and sigterm. Sigterm allows the child to clean up and then exit.

3. The test script verifies the correctness of the program by going through the list of tests that was pre-defined. It executes each command, reporting if it was a success or failure. If there are any failures, it exits the test and reports the failure. If hte test succeeds to the end, it prints that timeout is a success.

4. I created a shell script that ran timeout 300 times. All 300 times, it failed. I believe that this will consistently fail, because I believe that parent processes are executed before child processes. I believe this because the output for hte parent process was outputted before the child's process, even though they should have ran at hte same time.

Activity 02
-----------

1. I scanned the filesystem by os.walking through the directories list which is defined by the user (or default to '.'). By using os.walk, I could find each file inside each directory and subdirectory.

2. I loaded the rules by using the yaml function, yaml.load. After loading it into a list of dictionaries, rules, I parsed through the rules finding the patterns and appending that to a list of patterns. I then attempted to compile it into a regex expression using a try/catch and apprended to the regex list whether or not the compile was a success or failure. I then created another list with all the actions based on the patterns. For each file, I checked each pattern, using the same iterator to execute the action (pattern 0 executed action 0).

3. I used a dictionary to detect changes to files. I stored the timestamp of the file to the key that is the filename. In the current loop of the filesystem, if the timestamp is greater than the stored timestamp, the actions are ran, and the dictionary updated.

4. I executed each action by putting the splitting the command into a list and then storing this list into another list. I then used the iterator from the pattern the execute the correct action.

5.  Busy waiting is a problem where if the program checks for true cases, it may waste resources, slowing down the computer and the programs running including itself. Cache invalidation is a problem where, if a file is deleted, the program does not realize the file is deleted, and continues to compare the list of files to the previous list of files, including the deleted item. This wastes memory and time that could be used for other purposes. These can be alliviated by utilizing a real clock to keep track of time and deleting members of the dictionary of current files if a file is deleted.
