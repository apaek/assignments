#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHOMP(s)    (s[strlen(s)-1] = 0)

char *sanitize_string(char *s) {
  char *sanitized = (char*) malloc(strlen(s)+1);
  char *writer    = sanitized;
  char *reader    = s;
  
  while (*reader != 0) {
    if (isalpha(*reader)) {
      //printf("test");
      *writer = *reader;
      writer++;
    }
    reader++;
  }
  return sanitized;
}

bool is_palindrome(const char *s) {
  //printf("%s\n", s);
  const char *front = s;
  const char *back  = s + strlen(s)-1;
  float i=0;
  //printf("---%c--- ---%c--- %d %d", front[0], back[0], *front, *back);
  while (front[0] == back[0] && *front != *back) {
    front++;
    back--;
  }
  return (*front == *back);
}

int main(int argc, char *argv[]) {
  char buffer[100];
  char *sanitized, *result;
  while (fgets(buffer, BUFSIZ, stdin)) {
    //printf("-----%s %d", buffer, strlen(buffer));
    CHOMP(buffer);
    //printf("%s %d-----\n", buffer, strlen(buffer));
    sanitized = sanitize_string(buffer);
    //printf("test %s\n", sanitized);
    result    = is_palindrome(sanitized) ? "" : "not ";
    free (sanitized);
    printf("%s is %sa palindrome!\n", buffer, result);
  }

  return EXIT_SUCCESS;
}

