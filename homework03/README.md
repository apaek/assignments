Homework 03
===========

Activity 1
----------

  1. use `ls -lh` to see the size of the files
  
    -a. libgcd.so is larger because it has more flags when compiled, making it larger.
    
    -b. gcd-staic is larger because it is a static file that has all libraries downloaded into the program itself.

   2. gcd-static depends on no libraries because it has all library code copied into the program at compile time. This way, it is able to run independant of the libraries. To find out what libraries gcd-dynamic uses, use `ldd gcd-dyanmic`. The following libraries are used

          linux-vdso.so.1 =>  (0x00007fff005ba000)
          libgcd.so => not found
          libc.so.6 => /lib64/libc.so.6 (0x0000003ea5600000)
          /lib64/ld-linux-x86-64.so.2 (0x0000003ea5200000)


   3. The executable does not run. To run the executable, you need to set the environment variable `LD_LIBRARY_PATH` to our current directory that is holding libgcd.so and then exporting it. 

   4. Advantages of static linking: Staic linking allows you to make a program that works as a standalone once compiled. This will make execution faster.
   
   - Disadvantages of static linking: the files become much larger, using up more harddrive space.

   - Advantages of dynamic linking: Dynamic linking has much smaller file size, using less disk space.

   - Disadvantages of dynamic linking: programs cannot run without libraries. If they are moved or libraries not located in the library path, it will not execute.

Activity 2
----------

1. You can download the archive by using curl. Then using the tar command to unzip the file.
            
            curl -o ~/cse20189/assignments/homework03/palindrome https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/static/tar/is_palindrome.tar.gz
          
            tar zxf palindrome

2. I used the `-gdwarf-2` flag to force gcc to include debugging symbols.

3. For the Seg Fault, I used `gdb` to find the line at which it segfaults. Then I realized that the segfault happens because we try to access buffer's memory when it was not set.

    - For the Invalid Memory access, I used Valgrind to find out where it has memory access error. From the `Invalid read of size 1`, I realized that we're trying to access memory for a string that didn't have a \0 char.
  
    - For the Memory leak, I used valgrind to see that I never freed up memory that was malloc'd. So i free'd the memory after it was used.

4. The invalid memory access was the hardest to fix because it was difficult to understand where I was trying to access memory that Valgrind didn't approve of. We can avoid issues like this by properly providing the right memory when we change the size of a string (like with CHOMP)

Activity 3
----------
1. Contacting the COURIER:
    
        $ /afs/nd.edu/user15/pbui/pub/bin/COURIER `
  
  He had the following message:

             ________________________________________
            / Hmm... you sure you put the package in \
            \ the right place?                       /
              ----------------------------------------

2. Finding the package location:

        $ strace /afs/nd.edu/user15/pbui/pub/bin/COURIER`

  This output then showed the system calls the program ran, in particular
    
        stat("/tmp/%s.deaddrop", {st_mode=S_IFREG|0700, st_size=10485760, ...}) = 0

3. I realized that the courier wanted the file `/tmp/apaek1.deaddrop` so I proceeded to create it.

        vi /tmp/apaek1.deaddrop

4. After creating the file, I contacted the courier again. He replied with

             ______________________________________
            / Whoa whoa... you can't give everyone \
            \ access to the package! Lock it down! /
              --------------------------------------

5. I then changed user permissions so that only I could access the file.

        chmod 700

  The courier replied with
         _______________________________________
        / What are you trying to pull here? The \
        \ package is the wrong size!            /
         ---------------------------------------

6. So then I created random data so that the message would be large enough

        /dev/urandom of=/tmp/apaek1.deaddrop bs=1M count=10

    The courier then replied with

         ________________________________________
        / Well, everything looks good... I'm not \
        | sure what 'gS▒)▒▒(▒' means, but I'll   |
        \ pass it on.                            /
          ----------------------------------------

