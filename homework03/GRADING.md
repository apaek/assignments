Homework 03 - Grading
=====================

**Score**: 12.25 / 15

Deductions
----------
###Activity 1
#####Task 1

* -0.50 **--** Did not use pattern rule for object files (ie. %.o: %.c)
* -0.50 **--** *gcd-static* and *gcd-dynamic* should depend on both *main.o* and its respective *libgcd* file

#####Task 2

* **1.a** -0.25 **--** *libgcd.so* is larger because of the bookkeeping required for relocating shared libraries

###Activity 2
#####Task 1

* -0.25 **--** Should have *-g* and *-std=c99* flags for your *is_palindrome* rule

#####Task 2

* -0.25 **--** *valgrind* reports errors. Use *-v* or *--track-origin=yes* to see more info on them when running *valgrind*

#####Task 3

* **Q2** -0.25 **--** The *-g* flag is necessary for debugging
* **Q2** -0.25 **--** Did not mention that the these symbols increase the size of the executable
* **Q3** -0.50 **--** Needed to provide more information about your fixes. How did you fix the seg fault? What was the string for which there was an invalid memory access and how did you fix that? What memory did you have to free to fix the memory leak?

Comments
--------
###Activity 1

* You could have utilized the automatic variable *$@* much more!
* It is not necessary to have either *libgcd* file as a TARGET when they are prerequisites to the gcd executable targets
* Technically, each of *gcd-static*, *gcd-dynamic* and your *.so* target should use *$(LD)* instead of *(CC)*. And and *.o* targets should use *$(CC)*
