#!/bin/bash

#displays mascot for operating system


case $(uname) in
  Linux) echo "Tux";;
  Darwin) echo "Hexley";;
  FreeBSD) echo "Beastie";;
  NetBSD) echo "Beastie";;
  OpenBSD) echo "Beastie";;
  * ) uname
esac
