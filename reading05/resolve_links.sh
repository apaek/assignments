#!/bin/bash


for filename in $1/*; do
    result=
    if [ -h "$filename" ]; then
        result="$filename resolves to $(readlink $filename)"
        echo "$result"
    fi
done
