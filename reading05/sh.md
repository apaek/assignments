TLDR - sh
=========

Overview
--------

Shell scripts allow scripts to run commands on the commandline through a script


Examples
--------

creating variables
      
  - variables store information that can be accessed by name. They are created when a line contains the name of the variable immediately followed by an equals sign ("=") without spaces. The information the variable stores follows immediately after the equals sign.
  
  - Variable names must start with a letter, must not contain spaces, and must not contain punctuation marks

capturing STDOUT
  
  - To use the output of a command as an argument, you call the command inside parentheses following a dollarsign: ie `$(uname)`.

  - You can store the value of $(uname) in a varable. For example: `OS=$(uname)`

if statments
  
  - If statments are lines of code that only execute if a condition is met.

  - If statements are used by 
  
        if [condition]; then 
          [command] 
        fi

  - Else statements can follow if statements: 
  
        if [condition]; then 
          [command] 
        else 
          [command] 
        fi

  - Else if statements can also follow if statements: 
  
        if [condition]; then 
          [command] 
        elif [condition]; then 
          [command] 
        fi

case statement
  
  - Case statements are another form of flow control like if-statements.

  - Case statements can be used by:

        case [variable] in
          [value1] ) [command];;
          [value2] ) [command];;
        esac

for loop
  
  - For loops repeat a set of commands repeatedly until a condition, defined in the for loop, is met

  - For loops are created by:

        for [variable] in [words]; do
          commands
        done

functions
  
  - Functions are scripts of code called in main. This allows main to be more readable while still executing correctly

  - Functions are defined by a function-name followed by parentheses followed by brackets.

        functionName(){
        }

  - Functions can be called in main:
      
        $(functionName)

  - Once a function is called, it will execute the function and then return to the line after the function-call in main.

[trap](http://40.media.tumblr.com/4d830789d71eb44d81172b968ac5b9bc/tumblr_inline_nzfevykxev1rg0x34_500.jpg)

  - trap is a command that allows you to execute other commands when a signal is receieved.

  - It is used by

        trap [commands] [signals]

  - SIGKILL or signal 9 cannot ever be trapped because the process terminates once this signal is received.


Resources
=========
[Shell Script Blog](http://linuxcommand.org/lc3_writing_shell_scripts.php#contents)
