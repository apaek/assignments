#!/bin/bash

# hello.sh prints Hello {argument}

while [ "$1" != "" ]; do
  echo "Hello, $1!"
  shift

done

