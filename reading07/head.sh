#!/bin/sh
nopt='10'
while getopts ":hn:" opt; do
  case $opt in
    h) hopt=$opt;;
    n) nopt=$OPTARG;;
    *) echo "Invalid argument"
       echo "./head.sh -h for more information"
       exit 1
  esac
done

if [ ! -z $hopt ]; then
  echo "usage: head.sh

        -n N    Display the first N lines"
  exit 0
fi

awk -v num=$nopt 'NR<=num {print $0}'
  
