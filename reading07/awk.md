TDLR - awk
==========

Overview
--------

Awk is a pattern scanning and proccessing language. It scans for a specific pattern from an input and can execute code before finding the pattern (BEGIN), after finding the pattern, and at the end of the awk command (END).

Awk is used by:

        awk 'BEGIN {action;} /pattern/ {action;} END {action;}' input file

Examples
--------
  
  Print specific fields by using the $@, where @ is the field you wish to print
    
        print $1
        print $2

  Modify FS to control input field seperator. For example, to ":"

        awk 'BEGIN {FS=":";}'
  
  Using BEGIN and END
    
        awk 'BEGIN {print "write code that does stuff before awk executes";} END { print "write code that does stuff as awk finishes executing";}'

  Using pattern matching
    
    - match the pattern "pattern"

        awk '/pattern/ {print "pattern found" $1}'

    -match the pattern "name"

        awk '/name/ {print "The name is: " $2}'

  Using special variables such as NF and NR
    
    - Determine how many lines are in /etc/passwd
        
        cat /etc/passwd | awk 'END {print NR}'
    
    - Print the last field for each line of input

        cat /etc/passwd | awk 'BEGIN {FS=":"} {print $NF}'

  Using associative arrays to print the number of times a string appeared

        awk '/name/ {name[$2]++} END {for (i in name) print name[i], i}'

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/gawk.1.html)

[Associative Arrays](http://www.grymoire.com/Unix/Awk.html#uh-22)

[Basic Uses](https://www.digitalocean.com/community/tutorials/how-to-use-the-awk-language-to-manipulate-text-in-linux)
