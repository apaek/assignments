#!/bin/sh

holder=$1
URL=${holder:-'http://catalog.cse.nd.edu:9097/query.text'}


curl -s $URL | awk -v cpu=0 '/cpus/ {cpu+=$2} END {print "Total CPUs: "cpu}'

curl -s $URL | awk -v num=0 '/name/ {name[$2]++} END {for (i in name) {num=num+1} print "Total Machines: "num}'

curl -s $URL | awk -v num=0 -v name="" '/type/ {type[$2]++} END {for (i in type){if(type[i] > num){ name=i; num=type[i]}} print "Most Prolific Type: "name }'
