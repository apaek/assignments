#!/usr/bin/env python2.7

import sys,getopt
optn=10
stdin=0
if len(sys.argv) > 1:
  stdin=0
else:
  stdin=1

try:
  opts, args = getopt.getopt(sys.argv[1:], "hn:")
except getopt.GetoptError:
  print 'Invalid input. Use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt == '-h':
    print '''usage: head.py [-n NUM] files ...

    -n NUM  print the first NUM lines instead of the first 10'''
    sys.exit(0)
  elif opt == '-n':
    optn = arg
    if(len(sys.argv)>3):
      stdin=0
    else:
      stdin=1


for args2 in sys.argv:
  if args2 == '-':
    stdin=1
x=0
if(stdin):
  for line in sys.stdin:
    if int(x) < int(optn):
      print line,
      x+=1
    else:
      break
else:
  f=open(sys.argv[len(sys.argv)-1])
  for line in f:
    if int(x) < int(optn):
      print line,
      x+=1
    else:
      break;


