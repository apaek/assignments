#!/usr/bin/env python2.7

import sys, getopt

optc=0
optl=0
optw=0
stdin=0
dash =0
if len(sys.argv) == 2:
  stdin =1
else:
  stdin=0
if sys.argv[len(sys.argv)-1] == '-':
  stdin =1
  dash=1

try:
  opts, args = getopt.getopt(sys.argv[1:], "clw")
except getopt.GetoptError:
  print 'Invalid input. use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt == '-c':
    optc = 1
  elif opt == '-l':
    optl = 1
  elif opt == '-w':
    optw = 1
  elif opt == '-h':
    print '''usage: wc.py [-c -l -w] files ...

    -c      print the byte/character counts
    -l      print the newline counts
    -w      print the word counts'''
    sys.exit(0)
x=0
if(stdin):
  if optc:  
    for line in sys.stdin:
      x+= len(line)
  elif optl:
    for line in sys.stdin:
      x+=1
  elif optw:
    for line in sys.stdin:
      x += len(line.split())
  else:
    print 'error, needs at least one argument'
    sys.exit(1)
  if(dash):
    print str(x) + ' -'
  else:  
    print x
else:
  f = open(sys.argv[len(sys.argv)-1])
  if optc:  
    for line in f:
      x+= len(line)
  elif optl:
    for line in f:
      x+=1
  elif optw:
    for line in f:
      x += len(line.split())
  else:
    print 'error, needs at least one argument'
    sys.exit(1)
  print str(x) + ' ' + sys.argv[len(sys.argv)-1]
