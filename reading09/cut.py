#!/usr/bin/env python2.7

import sys, getopt

optd = '\t'
optf = 0
stdin = 0
d=0

try:
  opts, args = getopt.getopt(sys.argv[1:], "hd:f:")
except getopt.GetoptError:
  print 'Invalid input. Use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt == '-h':
    print '''usage: wc.py [-d DELIM -f FIELDS] files ...
    
    -d DELIM  use DELIM instead of TAB for field delimiter
    -f FIELDS select only these FIELDS'''
    sys.exit(0)
  elif opt == '-d':
    optd = arg
    d=1
  elif opt == '-f':
    optf = arg 
if len(sys.argv) == 3:
  stdin=1
else:
  if(d):
    if len(sys.argv) == 5:
      stdin=1
    else:
      stdin=0

if sys.argv[len(sys.argv)-1] == '-':
  stdin = 1

if optf==0:
  print 'Error, need to specify the fields'
  sys.exit(1)

fields=set(optf)
for i in fields:
  if i.isalnum():
    if (int(i) not in range(100)):
      fields.remove(i)
      break;
  else:
    fields.remove(i)
    break;
words=[]
thestring=''
if stdin:
  for lines in sys.stdin:
    lines = lines.rstrip('\n')
    words = []
    words = lines.split(optd);
     
    for i in range(100):
      if(str(i) in fields):
        thestring += words[i-1] + optd
    thestring += '\n'
else:
  f=open(sys.argv[len(sys.argv)-1])
  for lines in f:
    lines = lines.rstrip('\n')
    words=[]
    words = lines.split(optd);
    
    for i in range(100):
      if(str(i) in fields):
          thestring += words[i-1]+ optd
    thestring+= '\n'

thestring=thestring.rstrip('\n')
thestring = thestring.split('\n');
string=''
for line in thestring:
  string += line.rstrip(optd)+'\n'
string=string.rstrip('\n')
print string
    
    


