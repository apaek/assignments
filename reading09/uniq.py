#!/usr/bin/env python2.7

import sys,getopt
optc=''
try:
  opts, args = getopt.getopt(sys.argv[1:], "hc")
except getopt.GetoptError:
  print 'Invalid input. use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt == '-h':
    print '''usage: uniq.py [-c] files ...

    -c      prefix lines by the number of occurrence'''
    sys.exit(0);
  elif opt == '-c':
    optc=1
uniqLines = {0:''}
pair = 0
count = [1]
if optc==1:
  if len(sys.argv) == 3:
    if(sys.argv[2] == '-'):

      for line in sys.stdin:
        #strip newline char
        line = line.rstrip('\n');
        #if the line exists in previous line, incease count. Else add to list
        if line == uniqLines[pair]:
          count[pair]+=1
        else:
          pair+=1
          count.append(1)
          uniqLines[pair] = line
    else:
      f = open(sys.argv[2],'r')
      for line in f:
        #strip newline char
        line = line.rstrip('\n')
        #if the line exists in the previous line, increase count. else add to list
        if line == uniqLines[pair]: 
          count[pair]+=1
        else:
          pair+=1
          count.append(1)
          uniqLines[pair] = line
  elif len(sys.argv) == 2:
    for line in sys.stdin:
      #strip newline char
      line = line.rstrip('\n');
      #if the line exists in previous line, incease count. Else add to list
      if line == uniqLines[pair]:
        count[pair]+=1
      else:
        pair+=1
        count.append(1)
        uniqLines[pair] = line
  del uniqLines[0]
  del count[0]
  for word in uniqLines:
    print '{0:7d} {1}'.format(count[word-1], uniqLines[word])

else:
  if len(sys.argv) == 2:
    if(sys.argv[1] == '-'):
      for line in sys.stdin:
        #strip newline char
        line = line.rstrip('\n');
        #if line doesnt exist in previous line, add line to list
        if line != uniqLines[pair]:
          pair+=1
          uniqLines[pair] = line
    else:
      f = open(sys.argv[1],'r')
      for line in f:
        #strip newline char
        line = line.rstrip('\n')
        #if line doesnt exist in previous line, add line to list
        if line != uniqLines[pair]:
          pair+=1
          uniqLines[pair] = line
  elif len(sys.argv) == 1:
    for line in sys.stdin:
      #strip newline char
      line = line.rstrip('\n');
      #if line doesnt exist in previous line, add line to list
      if line != uniqLines[pair]:
        pair+=1
        uniqLines[pair] = line


  del uniqLines[0]

  for word in uniqLines:
    print uniqLines[word] 


