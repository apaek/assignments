TLDR - sed
==========

Overview
--------
sed is a stream editor for filtering and transforming text. It is unique in the sense that it can take input from piping.

It is used by: `sed [option] ... {script} [input-file] ...

It generally uses s and g as start and end of a specific command. For example:
        
        $ sed '/s/textToBeReplaced/textReplacing/g'

Examples
--------
To replace a string, failed, with another string, aced, from a piped command
      
        $ echo "I failed the test" | sed 's/failed/aced/g'

To replace multiple strings, text, with multiple other strings, strings

        $ sed 's/text1/string1/g; s/text2/string2/g; s/text3/string3/g'

To replace all leading whitespaces

        $ echo "            monekys love bananas" | sed -e 's/^[ \t]*//'

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/sed.1.html)
