TLDR - uniq
===========

Overview
--------
uniq reports or omits repeated lines. It filters matching lines from INPUT (default stdin) and writes them to OUTPUT (stdout).

It is used by: `uniq [option] ... [input [output]]`
Examples
--------
Print the (nonrepeated) lines from file1.list

      $ uniq file1.list

Print the repeated lines from file1.list

      $ uniq -d file1.list

Count the number of appearances of each line of file1.list

      $ uniq -c file1.list

Print only unique lines from file1.list
    
      $ uniq -u file1.list

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/uniq.1.html)
