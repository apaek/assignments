TLDR - tr
=========

Overview
--------
`tr` translates or deletes characters from stdin. By translates, it means replacing a current character with another. 

It is used by `tr [option] set1 [set2]`

Examples
--------
To capatalize all characters
  
      $ tr a-z A-Z
      [text]

To use with IO redirection

      $ echo "text" | tr a-z A-Z

To delete characters, example delete a

      $ tr -d a

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/tr.1.html)
