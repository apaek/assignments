TLDR - comm
===========

Overview
--------
comm compares two sorted files line by line. By sorted, it must be sorted lexically. To easily sort files correctly, use the sort command.

It is used by: `comm [option] file1 file2`

There are 3 main options, -1, -2, -3.

-1 prints lines unique to file 2 in first column

-2 prints lines unique to file 1 in first column

-3 prints lines that are unqiue to each file in their respective columns.

Examples
--------
Print only the lines in both file1 and file2.

        $ comm -12 file1 file2

Print the unqiue lines using -3

        $ comm -3 file1 file2

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/comm.1.html)
