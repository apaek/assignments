Reading 06
==========

1. To convert input text to uppercase we can use regex and the command tr.

        echo "all your base are belong to us" | tr a-z A-Z

2. To print the shell of the root use grep.
  
        cat /etc/passwd | grep root | grep -o '/bin/bash'

3. To find and replace words, use sed.

        echo "monekys love bananas" | sed 's/monekys/gorillaz/g'

4. To replace all instances of `/bin/bash`, `/bin/csh`, and `/bin/tcsh` to `/usr/bin/python` use sed

        cat /etc/passwd | sed 's/\/bin\/bash/\/usr\/bin\/python/g; s/\/bin\/csh/\/usr\/bin\/python/g; s/\/bin\/tcsh/\/usr\/bin\/python/g'

5. To replace all leading whitespaces use sed

        echo "           monekys love bananas" | sed -e 's/^[ \t]*//'

6. To find all files thathave a number staring with 4 and ending with 7 use grep

        cat /etc/passwd | grep "4[0-9]*7"

7. To follow the files file1.log, file2.log
      
        tail -f file1.log file2.log



8. Given file1 and file2, you can show the same lines by sorting and then using comm

        sort -o file1.tmp file1
        sort -o file2.tmp file2
        comm -12 file1.tmp file2.tmp
