TLDR - grep
===========

Overview
--------
grep searches the input for lines matching a specified pattern. By default, grep searches stdin and prints the matching lines to stdout.

It is used by: `grep [options] pattern [file]`

Examples
--------
To search through /etc/passwd for lines including the text root:

      $ cat /etc/passwd | grep root

To search through a file, like /etc/passwd, and print a specific string, like /bin/bash, if it exists:

      $ cat /etc/passwd | grep -o '/bin/bash'

To ignore case on a search of a string:

      $ grep -i [string]

To search through a file, /etc/passwd/, using regex

      $ cat /etc/passwd | grep -e [regex]

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/grep.1.html)
