TLDR - ping
===========

Overview
--------
  ping sends a packet to a host which then responds, indicating that the host is online.

  It is used by:

    - `ping [options] **destination**`

Examples
--------
  ping a server identified by 4.2.2.2
    
    - `ping 4.2.2.2`

  ping the student00 machine and produce a audible ping

    - `ping -a student00.cse.nd.edu`

  ping the student00 machine twice

    - `ping -c 2 student00.cse.nd.edu`

Resources
---------
[Manual](http://man7.org/linux/man-pages/man8/ping.8.html)
