TLDR - netstat
==============

Overview
--------

  netstat prints network connections, routing tables, interface statistics, masquerade connections, and multicast memberships

  It is used by :
    
    - `netstat [options]`

Examples
--------
  
  The basic command will list the open sockets of all configured address familes

    -`netstat`

  To display group membership info for IPv4 and IPv6

    - `netstat -g`

  To display information and explain it
    
    - `netstat -v`

Resources
---------
[Manual](http://man7.org/linux/man-pages/man8/netstat.8.html)
