TLDR - ifconfig
===============

Overview
--------
  ifconfig is used to configure the network interface. Its used uding boot time to set up interfaces as necessary.

  It is used by:
    
    - `ifconfig [options]`
Examples
--------

  if no options are given, it displays the current status of the interfaces.

    - `ifconfig`

  if you give a name of an interface, it will list the properties of that interface
    
    - `ifconfig eth0`

  if you wish to shut down a  driver, like eth0
    
    -`ifconfig eth0:0 down`

Resources
---------
[Manual](http://man7.org/linux/man-pages/man8/ifconfig.8.html)
