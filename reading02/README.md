Reading 02
==========

1. `cat uname > uname.txt`

2. 127.0.0.1. Look at the inet addr for local machine.
  
    -  `/sbin/ifconfig`

    - `/sbin/ip addr`

3. `host [host domain name]`

4. `ping [host name]`

5. You can securely transfer files by using either sftp or scp. For example, to send the file test.txt to my folder test in my home directory
  
    - `scp test.txt apaek1@student00.cse.nd.edu:/afs/nd.edu/user4/apaek1/test`

6. `ssh  [username]@[hostnmae]`

    - for example: `ssh apaek1@student00.cse.nd.edu`

7. `wget -O [directory/filename] '[file to download]'`
  
    - for example: `wget -O /afs/nd.edu/user4/apaek1/test/google.png 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png'`

8. `nmap [host name]`

