TLDR - traceroute
=================

Overview
--------
  traceroute tracks the route a packet takes from an ip to a given host

  it is used by:
    
    -`traceroute [options] **host** [packet_len]

Examples
--------
  To traceroute to ip 4.2.2.2

    -`traceroute 4.2.2.2`

  To traceroute to a specific port, like 21

    -`traceroute -p 21 4.2.2.2`

Resources
---------
[Manual](http://man7.org/linux/man-pages/man8/traceroute.8.html)
