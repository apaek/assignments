TLDR - scp
==========

Overview
--------
  scp stands for secure copy. It will copy a file between hosts on a network.
  scp is used by: 
    `scp [options] **file1** [host] **file2**`

Examples
--------
  copy a file test.txt into a directory called test in apaek1's home directory

    - `scp test.txt apaek1@student00.cse.nd.edu:/afs/nd.edu/user4/apaek1/test`

Resources
---------

- [Manual](http://man7.org/linux/man-pages/man1/scp.1.html)
