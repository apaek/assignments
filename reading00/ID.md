TDLR - id

Overview
  id is a command that prints the real and effective user and group IDs.

Examples
  id is used by: id [option]...[user]
    -Z, --context
      prings only the security context of the process
    -g, --group
      prints effective group ID
    -G, --groups
      prints all group IDs
    -n, --name
      prints a name instead of a number
    -r, --real
      prints the real ID
    -u, --user
      prints effective user ID
    --help
      displays help

Resources
  linux manual - http://man7.org/linux/man-pages/man1/id.1.html
