TDLR - who

Overview
  who - show who is logged on

Examples
  usage: who [option]... [FILE|ARG1 ARG2 ]
  
  who -a
    prints all -b, -d, --login, -r, -t, -T, -u
  who -b, who --boot
    prints time of last system boot
  who -d,who --dead
    prints dead processes
  who -H,who --heading
    prints line of column headings

References
  Linux Manual - http://man7.org/linux/man-pages/man1/who.1.html
