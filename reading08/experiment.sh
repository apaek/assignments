#!/bin/sh

./roll_dice.sh -r 1000 | 
awk '
BEGIN{
  #initialize arrays
  num[""]=0
}
/[1-6]/{
  #search for 1-6 and increment them in array
  num[$1]++
} 
END{
  #print output
  printf("1     %d\n", num[1]) 
  printf("2     %d\n", num[2])
  printf("3     %d\n", num[3])
  printf("4     %d\n", num[4])
  printf("5     %d\n", num[5])
  printf("6     %d\n", num[6])
}' > results.dat

