set terminal png
set output "results.png"
set grid
set boxwidth 0.95 relative
set style fill solid 0.5 noborder
set xrange [0:7]
set yrange [0:200]
plot "results.dat" w boxes lc rgb"blue" notitle
exit
