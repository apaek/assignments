#!/bin/sh
ropt='10'
sopt='6'
while getopts ":r:s:h" opt; do
  case $opt in
    r) ropt=$OPTARG;;
    s) sopt=$OPTARG;;
    h) hopt=$opt;;
    *) echo "Invalid argument"
            exit 1
  esac
done
numbers='1'
newline='
'
if [ ! -z $hopt ]; then
  echo "usage: roll_dice.sh [-r ROLLS -s sides]

      -r ROLLS        Number of rolls of die (default: 10)
      -s SIDES        Number of sides on die (default: 6)"
  exit 0
fi
for ((i=2;i<=${sopt};i++)); do
    numbers="$numbers${newline}$i"
done
j='0'
while [ $j -lt $ropt ]; do
  echo "$numbers" | shuf -n 1
  j=$(($j+1))
done


