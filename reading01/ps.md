TLDR - ps
=========

Overview
--------

  *Ps* reports a snapshot of the current processes.
  
  It is used by:

  - ps [options]

Examples
--------

  ps
  
  - displays information about a selection of active processes

  ps -e, ps -ef, ps -eF, ps -ely

  - displays every process running on the system

  ps p [pidlist], ps -p [pidlist] 

  - displays the specific processes defined by pidlist

  ps -G [grouplist]
  
  - displays the process by specified groups

Resources
---------

- [Manual](http://man7.org/linux/man-pages/man1/ps.1.html)
