TLDR - write
============

Overview
--------

  *Write* allows you to send a message to another user

  It is used by:

  - write [user] [ttyname]
  
  To end the message you need to send an EOF, or can press CTRL+C to end.

Examples
--------

  write [user]
  
  - indicates that you will send a message to the specified user. The following lines will be sent as messages.

  write [user] [ttyname]

  - The ttyname indicates which terminal to send the message to, in the situation where the user is logged in at multiple terminals.

Resources
---------

- [Manual](http://man7.org/linux/man-pages/man1/write.1.html)
