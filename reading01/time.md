TLDR - time
===========

Overview
--------

  *Time* is a command that gives the resource usage of a program

  It is used by:
  
  - time [options] [command] [arguments]

Examples
--------

  time [command]

  - this will tell how long it takes to run the command specified
  - command can be a unix command or an executable.
 
References
----------

- [Manual](http://man7.org/linux/man-pages/man1/time.1.html)
