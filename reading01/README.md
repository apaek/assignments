Reading 01
==========
 
1. To make a file private to myself, I need to change the owner and group of the file to myself. Then I would need to change permissions so that others cannot read it.  
    - chown apaek1: [filename]
    - chmod o-r [filename]


2. To make a symbolic link in your home directory, first be in your home directory then execute
  
    - ln -s /afs/nd.edu/coursesp.16/cse/cse20189.01 [linkname]

3. 
By using *ls* command, you can find the size of a file right after the group and before the date created. Similarly you can use *du* to find an estimation of the size of the file.
  
    - ls -l BigFile
    - du -h BigFile

4. To find the size of a directory use *du*
  
    - du -h MyFolder

5. The PID of the ssh student00 process is 25263 so we can terminate it by using the *kill* command 
  
    - kill -15 25263

6. We can terminate multiple process by just listing multiple pids with a space seperating them in the *kill* command.
  
    - kill -15 25129 25913

7. To determine how long simulation runs, we just use the command *time*.
  
    - time simulation

8. To set the default editor to vim, the editor I personally use, I use the following commands because my shell is tcsh.
  
    - setenv VISUAL vim 
    - setenv EDITOR vim

