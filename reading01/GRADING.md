Reading01 - Grading
==================
**Score** = 3.5/4

Deductions
---------
-0.5 Question 8

Comments
--------
To make the default editor a persistent change, you need to put
this command in a rc file, such as .cshrc

EDIT: rubric states to deduct .5 for incorrect responses, not .25
