TLDR - du
=========

Overview
--------

*du* is a command that estimates a size of a file

It is used by:

-    du [OPTION] ... [FILE]...

Examples
--------

  du

-     prints the sizes of directories in the current directory

  du [FILENAME]

-     prints the size of [FILENAME]

  du -h

-     prints the sizes of directories in the current directory in readable format, using K, M, G for sizes of files

du -h [FILENAME], du --human-readable [FILENAME]

-     prints the file size in readable format, using K, M, G for sizes of files

  du -a, du --all

-     prints sizes of all files including directories


Resources
---------

- [Manual](http://man7.org/linux/man-pages/man1/du.1.html)
