TLDR - ls
==========

Overview
--------
*ls* is a command that lists the directory's contents.

It is used by:
  ls [option] ... [file]

Examples
--------
  ls -l
    
-    prints the directory in a vertical list
  
  ls -a, --all

  -  prints a list of all contents, including files starting with .
  
  ls -s

-    prints the size of each file


Resources
---------
- [Manual](http://man7.org/linux/man-pages/man1/ls.1.html)

