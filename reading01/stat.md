TDLR - stat
===========

Overview
--------

*Stat* is a command htat displays the file or the file system status.

It is used by:
  
  stat [OPTION] ... [FILE] ...

Examples
--------
  
  stat [FILE]

-   prints the default stat info

  stat -c [format] [FILE], stat --format [format] [FILE]

-   prints the specified format instead of default
    
  stat -c %g [FILE] 

-    prints the group ID of the owner

  stat -c %n [FILE}

-    prints the file name

  stat -t, stat --terse

-    prints the info in terse form.

Resources
---------
- [Manual](http://man7.org/linux/man-pages/man1/stat.1.html)
