TLDR - tar
==========

Overview
--------

  Tar is a archiving program designed to store multiple files in a single file and to manipulate such archives. 

  Tar is invoked by:
  
    - tar **options** [file]


Examples
--------
  To archive a directory

    - tar -cvf [filename] /directory/being/archived

  To extract a tar file

    - tar -zxf [filename]

  To list the contents of an archive
    - tar -t [archivename]

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/tar.1.html)
