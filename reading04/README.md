Reading 04
==========

1. `tar -zxf file.tar.gz`

2. `tar -cvf data.tar.gz /directory/to/data`

3. `unzip file.zip`

4. `zip -r data.zip /directory/to/data`

5. `apt-get install **package**`: For example, for the package zip `apt-get install zip`

6. `yum install **package**`: For example, `yum install ocfs2-tools`

7. You need to download python, extract the tar file, then use make to install it.

        curl -o **/destination/directory/**python.tar https://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz     
        tar -xzf python.tar
        cd Python-2.7.11
        ./configure
        make
        make install

8. `cat **filename** | curl -F 'sprunge=<-' http://sprunge.us` 


9. The `sudo` command allows you to run a command as a superuser, aka root. It is invoked by `sudo **command**`.


