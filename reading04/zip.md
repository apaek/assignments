TLDR -zip
=========

Overview
--------

zip is a compression and file packaging utility. It will compress files into a smaller zip file which can be transfered and then extracted by using unzip.

Zip is invoked by:
  
  - zip **options** [zipfilename] [filestozip]

Examples
--------
  To zip the current directory and subdirectories to zipfile recursively

    - zip -r zipfile *

  To zip a single file, data.txt to zipfile
    
    - zip zipfile data.txt

  To zip files in the current folder to zipfile

    - zip zipfile *

Resources
---------
[Manual](http://linux.die.net/man/1/zip)
