TLDR - sudo
===========

Overview
--------
  Sudo allows a permitted user to execute a command as another user. Sudo is useful because it allows you to execute single commands, preventing accidents that may delete the entire harddrive. It requires login information to affirm the user is permitted.

  sudo is invoked by:

    - sudo [options] [command]
Examples
--------
  To execute a command, like ls, as superuser

    - sudo ls
  
  To execute command ls as part of group students

    - sudo -g students ls

  To get a list of allowed and forbidden commands for the user

    - sudo -l

Resources
---------
[Manual](https://www.sudo.ws/man/sudo.man.html)
