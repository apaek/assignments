TLDR - unzip
============

Overview
--------
  Unzip will mainipulate a zip archive. It can list, test, or extract files.
  
  To invoke unzip, use

    - unzip [options] [file]

Examples
--------
  To unzip zipfile.zip to the current dirctory

    - unzip zipfile.zip

  To unzip all zip files in a current directory
      
    - unzip "*.zip"

  To list the files in the zip zipfile.zip

    - unzip -l zipfile.zip

Resources
---------
[Manual](http://linux.die.net/man/1/unzip)
