TLDR - sprunge
==============

Overview
--------
  Sprunge is a command line pastebin. It will copy the output of a command and paste it to an online pastebin hosted by http://sprunge.us

  To invoke sprunge:

    - [command] | curl -F 'sprunge=<-' http://sprunge.us
Examples
--------
  To paste the code from test.c to a pastebin:

    - cat test.c | curl -F 'sprunge=<-' http://sprunge.us

  To paste the list of directories and files from ls in the current directory

    - ls | curl -F 'sprunge=<-' http://sprunge.us
Resources
---------
[Manual](http://sprunge.us/)
