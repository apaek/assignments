#!/usr/bin/env python2.7

import sys,getopt,os

inFD=0
outFD=1
count = sys.maxint
bs = 512
seek = 0
skip = 0

def open_fd(path,mode):
  try:
    return os.open(path,mode)
  except OSError as e:
    print >>sys.stderr, 'Could not open file {}:{}'.format(path,e)
    sys.exit(1)

def read_fd(fd,n):
  
  try:
    return os.read(fd,n)
  except OSError as e:
    print >>sys.stderr, 'Could not read from file descriptor {}: {}'.format(fd,e)
    sys.exit(1)

def write_fd(fd,string):
  try:
    return os.write(fd,string)
  except OSError as e:
    print >>sys.stderr, 'Could not write to file descriptor {}: {}'.format(fd,e)
    sys.exit(1)

def close_fd(fd):
  try:
    os.close(fd)
  except OSError as e:
    print >>sys.stderr, 'Could not close file descriptor {}: {}'.format(fd,e)
    sys.exit(1)

def lseek_fd(fd,pos):
  try:
    return os.lseek(fd,pos,0)
  except OSError as e:
    print >>sys.stderr, 'Could not change position in file descriptor {}: {}'.format(fd,e)
    sys.exit(1)

#parse arguments
options=[]
for arg in sys.argv[1:]:
  options=arg.split('=')
  if options[0] == 'if':
    inFD=open_fd(options[1], os.O_RDONLY)
  elif options[0] == 'of':
    outFD=open_fd(options[1], os.O_WRONLY|os.O_CREAT)
  elif options[0] == 'count':
    count = int(options[1])
  elif options[0] == 'bs':
    bs = int(options[1])
  elif options[0] == 'seek':
    seek = int(options[1])
  elif options[0] == 'skip':
    skip = int(options[1])

copiedStr=''
i=1
copiedStrLen=0;
#skip or seek
if seek:
  lseek_fd(outFD,seek*bs)
if skip:
  lseek_fd(inFD,skip*bs)
#output
while (i<=count):
  copiedStr = read_fd(inFD,bs)
  copiedStrLen=write_fd(outFD,copiedStr)
  if copiedStrLen == 0:
    break;
  i+=1

