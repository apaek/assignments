Homework 07
===========

Part 1
------

1. I parsed command line arguments using a for loop and an empty list. The command is then split using the '=' as a delimiter. It is then tested in a group of if-elif statements that then sets global variables so that the argument is executed.

2. I opened the input and output files by using os.read. I created a try, catch function for all os commands so that if any attempt fails, it will output what failed and what.

3. I utilized the seek and skip arguments by using lseek. Since I stored the number the user wanted to seek/skip, I could easily use lseek.

4. I utilized bs to read in specific amount of bytes from the if using the os.read function, saved this into a temp string, then wrote the temp string into the of. This was all put into a while loop that executed up to count times or broke if there was nothing copied.

Part 2
------

1. I parsed the arguments by using a forloop tha ran the amount of times as there are arugments. It then parsed through each argument, compared it to the predefined arguments using if-elif statements, and then set a global variable if the argument was found.

2. I walked the directory tree by using os.walk. I made sure to set the followlinks variable true so that it would follow any symbolic links if possible.

3. I determined whether or not a file should be printed by using a function that tested all the global variables (arguments) that were set. If any file should not be written, the function returned false and it was not added to the final list of outputs. If it reached the end of the function, the fileopbject was added to the list.

4. Find seems to use a function called newfstatat on directories to get file information about the files inside the directories while my implementation of find ses stat on each and every file including directories. This seems to do much less work by being more efficient.
