#!/usr/bin/env python2.7

import getopt
import os
import sys
import stat
import fnmatch
import re
# Global Variables

PROGRAM_NAME = os.path.basename(sys.argv[0])
dpath='.'
ftype=''
executable=0
readable=0
writable=0
checkEmpty=0
name=''
pattern=''
regex=''
perm=''
ntFile=''
uid=''
gid=''

# Functions

def error(message, exit_code=1):
    print >>sys.stderr, message
    sys.exit(exit_code)

def usage(exit_code=0):
    error('''Usage: {}

Options:

    -h     Show this help message'''
    .format(PROGRAM_NAME), exit_code)

def stat_fd(path):
  try:
    return os.stat(path)
  except OSError as e:
    #print >>sys.stderr, 'Could not os.stat {}: {}'.format(path, e)
    '''holder'''
  try:
    return os.lstat(path)
  except OSError as e:
    #print >>sys.stderr, 'Could not os.lstat {}: {}'.format(path,e)
    '''holder'''
def listdir_fd(path):
  try:
    return os.listdir(path)
  except OSError as e:
    return ['a']
#def 
def include(path):

  '''returns tue if item should be included in outut, otherwise false'''
  st = stat_fd(path)
  
  if ftype == 'f':
    if stat.S_ISREG(st.st_mode) == 0:
      return False
  elif ftype=='d':
    if stat.S_ISDIR(st.st_mode) == 0:
      return False
  if executable:
    if os.access(path, os.X_OK)==0:
      return False
  if readable:
    if os.access(path, os.R_OK)==0:
      return False
  if writable:
    if os.access(path, os.W_OK)==0:
      return False
  if checkEmpty:
    if stat.S_ISDIR(st.st_mode):
      if listdir_fd(path):
        return False
    elif st.st_size:
      return False
  if name:
    if not(fnmatch.fnmatch(os.path.basename(path), name)):
      return False
  if pattern:
    if fnmatch.fnmatch(path,pattern)==False:
        return False
  if regex:
    tmpstr = re.findall(regex,path)
    if tmpstr:
      if not(tmpstr[0] == path):
        return False
    else:
      return False
  if perm:
    if oct(stat.S_IMODE(st.st_mode))[1:] != perm:
      return False
  if ntFile:
    if dpath == '.':
      tmpntFile = dpath+'/'+ntFile
    else:
      tmpntFile=ntFile
    tmp =stat_fd(tmpntFile)
    if st.st_mtime <= tmp.st_mtime:
      return False
  if uid:
    if st.st_uid != int(uid):
      return False

  if gid:
    if st.st_gid != int(gid):
      return False
  return True
# Parse Command line arguments
'''try:
    options, arguments = getopt.getopt(sys.argv[1:], "h")
except getopt.GetoptError as e:
    error(e)

for option, value in options:
    if option == '-h':
        usage(0)
    else:
        usage(1)
'''
for i in range(len(sys.argv)):
  if sys.argv[i] == "-type":
    ftype = sys.argv[i+1]
  elif sys.argv[i] == "-executable":
    executable=1
  elif sys.argv[i] == "-readable":
    readable=1
  elif sys.argv[i] == "-writable":
    writable=1
  elif sys.argv[i] == "-empty":
    checkEmpty=1
  elif sys.argv[i] == "-name":
    name=sys.argv[i+1]
  elif sys.argv[i] == "-path":
    pattern = sys.argv[i+1]
  elif sys.argv[i] == "-regex":
    regex=sys.argv[i+1]
  elif sys.argv[i] == "-perm":
    perm=sys.argv[i+1]
  elif sys.argv[i] == "-newer":
    ntFile=sys.argv[i+1]
  elif sys.argv[i] == "-uid":
    uid=sys.argv[i+1]
  elif sys.argv[i] == "-gid":
    gid = sys.argv[i+1]

# Main Execution
if len(sys.argv) > 1:
  if sys.argv[1][0] == '/':
    dpath=sys.argv[1]

tmpFiles=[dpath]
files=[]
for dirpath, dirnames, filenames in os.walk(dpath,followlinks=True):
  for subdirname in dirnames:
    #if(include [os.parth.join
    tmpFiles= tmpFiles+ [os.path.join(dirpath, subdirname)]
  for filename in filenames:
    tmpFiles = tmpFiles + [os.path.join(dirpath,filename)]
for i in tmpFiles:
  if include(i):
    files.append(i)
for i in files:
  print i
#error('{} Not implemented!'.format(PROGRAM_NAME))
