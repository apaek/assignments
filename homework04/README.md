Homwork 04
==========

Activity 1
----------

1. I implemented bake.sh by using a forloop to check through the files with the SUFFIXES in the current directory. Any found files were compiled.

    - a. I handled it by using if statements to see if it exists. I wanted to practice using parameter extension as well, so CFLAGS was set using parameter extension.

    - b. I iterated over all files with the SUFFIXES in the current directory by using a forloop that checked anything `*$SUFFIX`. This used the wildcard to only go through files with the suffix.

    - c. I handled the VERBOSE variable by setting the value of VERBOSE at the beginning using an if statement. When I compiled the source code, if VERBOSE=1 then I echoed the compiling command.

    - d. If a file failed to compile, it would exit by using short circuit logic. It checks the value returned by the compiler (0 for success and 1 for failure) and then nots it (!), so that the exit only runs if the compiling failed.

2. bake.sh is limited in the fact that it can't compile .h files into the executables. I could include this, but since make already includes it, and I am used to make, I will continue to use make

Activity 2
----------
1. I implemented disk_usage.sh script by using getopts to get which flags are included. I then used an if statement to test if the -a flag was included. If it was, then I included the -a flag in du. If it wasn't, I didnt. I then piped each command to each other.
  
    - a. I parsed the command line arguments by first checking for flags uisng getopts. I then used a for loop to check each argument to see if it was a directory. If it was, I then called the du, sort, and head commands for the directories.

    - b. If there are no command line arguments, the program has no directory to check, and therefore will not do anything. If there are no flags but a directory, the case inside the while loop for the getopts will never run, so the if statement can tell that the -a flag was not included.

    - c. Since I use a forloop to go through each argument to check if it is a directory, the listing executes for each directory.

    - d. To list only the top N items in each directory, I used du -h[a] to find the sizes of all the directories [and files if applicable]. I then piped this to sort -hr to sort the files by size in reverse order, so the largest appears first. Then piped that to head -n [N] to print only the top N items.

2. The hardes part of this script was piping the commands correctly. I tried multiple methods, including using variables and $(command) to run the command. The part of the program that took up the most amount of code was the piping because it was very long. This isnt really surprising, because most of the code was checking if certain conditions were true.

Activity 3
----------
1. I implemented the taunt.sh by creating functions that display a message using cowsay and then exit. These functions are then called by traps, depending on which signal was sent.
  
    - a. The different signals were handled by trap. trap would then call a function depending on which signal was sent.

    - b. I passed messages to cowsay using echo and a pipe `echo [message] | cowsay`

    - c. I handled the timeout by incrementing a variable slept for every second the sleep command executed. If it executed all 10 seconds, it would write the timeout message.

2. As of now, since I am more used to C programs, I believe that C programs are easier. I prefer C programs because I'm more used to it and it has symbols (brackets, perenthesis, etc) that make sense. I would use C for most cases over shell script. however, if I had to send multipe commands to the shell, using shell script would be easier. 

