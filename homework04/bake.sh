#!/bin/sh

# bake.sh automatically compiles any source code files in the current directory

if [ ! -z $SUFFIXES ]; then
  false
else
  SUFFIXES=.c
fi


if [ ! -z $CC ]; then
  false
else
  CC=gcc
fi


holder="-std=gnu99 -Wall"
CFLAGS=${CFLAGS:=$holder}


if [ ! -z $VERBOSE ]; then
  false 
else  
  VERBOSE=0
fi
for filename in *$SUFFIXES; do
  file="$(basename $filename $SUFFIXES)"
  [ !$($CC $CFLAGS -o $file $filename) ] || exit 1  
  if [ $VERBOSE = 1 ]; then 
    echo "$CC $CFLAGS -o $file $filename"
  fi
done

