#!/bin/sh


nopt=10
while getopts ":an:" opt; do
  case $opt in
    a) aopt=$opt;;
    n) nopt=$OPTARG;;
    *) echo "Invalid Argument"
       exit 1
  esac
done

if [ ! -z $aopt ]; then
  for args in $@; do
    if [ -d $args ]; then
      du -ha $args 2> /dev/null | sort -hr 2> /dev/null | head -n $nopt  2> /dev/null
    fi
  done
else
  for args in $@; do
    if [ -d $args ]; then
      du -h $args 2> /dev/null | sort -hr 2> /dev/null | head -n $nopt 2> /dev/null
    fi
  done
fi
