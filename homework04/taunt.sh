#!/bin/sh

PATH=$PATH:/afs/nd.edu/user15/pbui/pub/bin
echo "What do you want...? I'm tired..." | cowsay
slept=0

angry(){
  echo "WTF?! You deserve a slap! GTFO bro" | cowsay
  exit 0
}
happy(){
  echo "Ah, I wanna keep sleeping. Why are you waking me now...? Oh, alright I'll help you out" | cowsay
  exit 0
}
trap happy SIGHUP

trap angry SIGTERM SIGINT
until [ $slept = 10 ]; do
  sleep 1
  slept=$(($slept + 1))
done

if [ $slept = 10 ]; then
  echo "Alright, bedtime~ " | cowsay
fi

