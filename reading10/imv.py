#!/usr/bin/env python2.7

import sys,getopt,os, tempfile, fnmatch
path ='.'

try:
  opts, args = getopt.getopt(sys.argv[1:], "h")
except getopt.GetoptError:
  print 'Invalid option. Use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt =='-h':
    print 'Usage: imv.py FILES...'
    sys.exit(0)
curDirFiles=os.listdir(path)
files=[]
for args in sys.argv[1:]:
  for i in curDirFiles:
    if fnmatch.fnmatch(i, args):
      files.append(i)

f=tempfile.NamedTemporaryFile(delete=False)

for i in range(len(files)):
  f.write(files[i]+'\n')
f.close()

try:
  os.system('$EDITOR ' +  f.name)
except:
  print 'error'
fp=open(f.name)
tmpString = fp.read()
changedFiles=tmpString.split('\n')
changedFiles.pop()
zipped = zip(files,changedFiles)
for x,y in zipped:
  os.rename(x,y)
os.unlink(f.name)
