#!/usr/bin/env python2.7

import sys,getopt,os,time
optN=2.0
n=0
command=' '
try:
  opts, args = getopt.getopt(sys.argv[1:], "hn:")
except getopt.GetoptError:
  print 'Invalid input. Use -h for help'
  sys.exit(1)
for opt, arg in opts:
  if opt == '-h':
    print '''Usage: watch.py [-n INTERVAL] COMMAND
    
    Options:

    -n INTERVAL   Specify update interval (in seconds)'''
    sys.exit(0)
  elif opt=='-n':
    optN = float(arg)
    n=1

if n:
  command=command.join(sys.argv[3:])
else:
  command=command.join(sys.argv[1:])


while(1):
  try:
    os.system('clear')
    print 'Every '+ str(float(optN)) + 's: ' + command
    os.system(command)
    time.sleep(optN)

  except:
    sys.exit(0)
