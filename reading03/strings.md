TLDR - strings
===========

Overview
--------
  
  The GNU strings prins the strings of printable characters in a file.
  
  It can be used by:

        - strings [options] **filename**


Examples
--------
  
  Print hello.txt using default strings, which only prints character sequences of length 4 or longer.
  
        - strings hello.txt

  Change the minimum length of sharacter sequences to be printed to 8

        - strings -n 8 hello.txt

  Print the name of the file before each line of text

        - strings -f hello.txt

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/strings.1.html)
