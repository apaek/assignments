TLDR - gcc
==========

Overview
--------
  
  Gcc is the compiler of C and C++. 

  It is used by:
        
        - gcc [options] **filename**

Examples
--------

  Compiling basic C program, hello.c, to default executable, a.out
      
        - gcc hello.c

  Compiling basic C program, hello.c, to a executable with name Hello.

        - gcc -o Hello hello.c

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/gcc.1.html)
