TLDR - make
===========

Overview
--------
  
  A GNU utility to maintain a group of programs. Requires a makefile that gives the commands necessary to recompile the programs.

  It is used by:
      
      - make [options] [target]

Examples
--------
  
  An example of makefile code that compiles the programs hello.c, and main.cinto the executable main.

      - main: main.o hello.o
          gcc main.o hello.o -o main
        main.o: main.c
          gcc -c main.c
        hello.o: hello.c
          gcc -c hello.c
  
  To print debugging information while compiling using a makefile.
    
      - make -d

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/make.1.html)
[Tutorial](http://mrbook.org/blog/tutorials/make/)
