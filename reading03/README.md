Reading 03
==========

1. ls -al

2. ldd /bin/ls

3. strace ls

4. gdb hello-debug

5. valgrind --tool=memcheck hello-dynamic

6. gprof hello-profile
