TLDR - strace
=============

Overview
--------
  Strace traces system calls and signals, running the specified command until it exits. It intercepts and records the system calls, printing these calls if asked to.

  It is used by :

        - strace [options] **command**

Examples
--------
  Strace the command ls

        - strace ls
  
  Determine the countime, calls, and errors for each system call and print a report after exiting for ls.

        - strace -c ls

  Print relaive timestamps to each system call for ls.

        - strace -r ls
  
  Print the real timestamps to each system call for ls

        - strae -t ls

Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/strace.1.html)
