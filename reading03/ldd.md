TLDR - ldd
==========

Overview
--------

  ldd prints shared object (library) dependancies required by the program.

  It is used by:

        - ldd [option] **file**

Examples
--------

  To just list the shared objects for file hello-dynamic

        - ldd hello-dynamic

  To print more information, including version information for hello-dynamic

        - ldd -v hello-dynamic


Resources
---------
[Manual](http://man7.org/linux/man-pages/man1/ldd.1.html)
