Homework 01
===========
Exercise 01
-----------
  1. `/afs/nd.edu/user14/csesoft`

  2. `cd ../csesoft`

  3. `cd ~/../csesoft`

  4. `ln -s /afs/nd.edu/user14/csesoft csesoft`

Exercise 02
-----------
  1. assume you are in home directory

        mkdir images
     
        cp -R /usr/share/pixmaps/  /afs/nd.edu/user4/apaek1/images
        
  
  2. `ls -L` will list all the contents of the directory. Any links that are red should be broken. There are indeed broken links
  
  3. Took 0 seconds
     
     `mv images pixmaps`
  
  4. It took .001 seconds. It is slower, probably because it has to go through two directories
     
     `cp -R /afs/nd.edu/user4/apaek1/pixmaps /tmp/apaek1-pixmaps.`
     
  5. It took .001 seconds, the same time as the previous command
     
     `rm -R /tmp/apaek1-pixmaps`

Exercise 03
-----------
  1. `ls -lh /afs/nd.edu/user37/ccl/software`
  
  2. `ls -lt /afs/nd.edu/user37/ccl/software`
  
  3. 1937 files
     
        du /afs/nd.edu/user37/ccl/software/cctools/x86_64 -a | wc -l
     
        du /afs/nd.edu/user37/ccl/software/cctools/x86_64 | wc -l
    
    The first line gives total number of files and directories (2026), while the second gives total number of directories (89). Subtraction determines total files (excluding directories).
  
  4. Yes this file exists
     
     `find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -name 'weaver' `
  
  5. redhat5 is the largest with 77MB
     
        du -h osx-10.9 
     
        du -h redhat5
     
        du -h redhat6
  
  6. 768 files
     
     `find /afs/nd.edu/user37/ccl/software/cctools/x86_64/redhat5 -type f | wc -l`
  
  7. the largest file is parrot_run_hdfs which is 18M
    
    `find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -type f -exec du -ah {} + | sort -h`
  
  8. 1937

    `find /afs/nd.edu/user37/ccl/software/ccools/x86_64 -type f -mtime +29 | wc -l`
  
Exercise 04
-----------

  1. the user, group, and everyone can execute the file. The user and group can read the file. Only the user can edit the file.

  2. a. `chmod u-x,g-rx,o-x data.txt`
     
     b. `chmod 770 data.txt`
     
     c. `chmod 444 data.txt`
    
     d. `chmod a-rwx`

  3. Anyone with write permissions on the directory data.txt is inside can delete the file.

Exercise 05
-----------

  1. home has:
        
        Access list for . is
      
        Normal rights:
        
          nd_campus l
        
          system:administrators rlidwka
        
          system:authuser l
        
          apaek1 rlidwka

     Private has:
        
        Access list for . is
        
        Normal rights:
          
          system:administrators rlidwka
          
          apaek1 rlidwka
     
     Public has: 
      
        Access list for . is
      
        Normal rights:
        
          nd_campus rlk
          
          system:administrators rlidwka
          
          system:authuser rlk
        
          apaek1 rlidwka
      
    The letters after the user's name represent their rights.
        
          r = read (allows user to read contents of files and use ls -l)
          
          l = look up (can use ls, ls -ld, fs listacl)
          
          i = insert (allows users to add files to directory)
          
          d = delete (allows user to remove files and subdirectories)
          
          w = write (allows user to modify contents of files and use chmod)
          
          k = lock (allows user to run programs)
          
          a = administer (allows user to change the directory's ACL)
  
  2. The folder /afs/nd.edu/common has:
  
        Access list for . is
        
        Normal rights:
          
          nd_campus rl
          
          system:administrators rlidwka
            
          system:authuser rl
        
    When I tried to create a apaek.txt, it told me that this file system is read only, implying I don't have the privilleges to make a file.

  3. `fs setacl [directory] pbui [permission]`
     
     An example for the folder test in the home directory
    
    `fs setacl test pbui write`

Exercise 06
-----------

  The permissions for each file are:
  
        -rw-rw-rw- 1 apaek1 dip 0 Jan 22 16:11 world1.txt
      
        -rw-r--r-- 1 apaek1 dip 0 Jan 22 16:11 world2.txt
      
        -rw--w--w- 1 apaek1 dip 0 Jan 22 16:11 world3.txt
  
  The permissions for these files are not the same. umask removes permissons from the default permissions for files.

  `umask 000` keeps default permissions for files (666), which give the user, group, and other read and write permissions
  
  `umask 022` keeps the default permissions for the user and removes the write permissions from group and other. The user has read and write permissions, while group and other have only read permissions.

  `umask 044` keeps the default permissions for the user and removes the read permissions for the group and other. The user has read and write permissions while the group and other have only write permissions.
    




