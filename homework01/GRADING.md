Homework 01 - Grading
====================

**Score**:  13.5 / 15

Deductions
----------

Exercise 2;
-0.25 (4)
Exercise 5:
-0.5 (2)

Comments
--------

Exercise 2;
(4) This is slower because we are moving the files to a different filesystem and thus need to perform an actually copy (and not just a rename)

Exercise 3:
(3) Alternatively: find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -type f | wc -l
(4) Use the -executable flag to make sure you find an executable
(5) Alternatively: du -sh /afs/nd.edu/user37/ccl/software/cctools/x86_64/* | sort -rn
(7) Alternatively: find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -type f | xargs du -h | sort -rh | head
(8) Number should be around 356

Exercise 4:
(3) Or the root user

Exercise 5:
(2) /afs/nd.edu/common has Unix permissions `drwxrwxrwx`, but we can’t touch a file b/c of AFS ACLs.  This means AFS supersedes or overrides Unix permisions
