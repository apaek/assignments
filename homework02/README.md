Homework 02
===========

Activity 1
----------

1.                  #Create workspace on source machine: student00
                    mkdir /tmp/apaek1-workspace
                    #Generate 10MB file full of random data
                    dd if=/dev/urandom of=/tmp/apaek1-workspace/10MB bs=1M count=10

                    #create 10 hard links to 10MB file
                    ln /tmp/apaek1-workspace/10MB data0
                    ln /tmp/apaek1-workspace/10MB data1
                    ln /tmp/apaek1-workspace/10MB data2
                    ln /tmp/apaek1-workspace/10MB data3
                    ln /tmp/apaek1-workspace/10MB data4
                    ln /tmp/apaek1-workspace/10MB data5
                    ln /tmp/apaek1-workspace/10MB data6
                    ln /tmp/apaek1-workspace/10MB data7
                    ln /tmp/apaek1-workspace/10MB data8
                    ln /tmp/apaek1-workspace/10MB data9

                    #create workspace on target machine: student01
                    mkdir /tmp/apaek1-workspace


  
2.  11M. No this is not surprising b/c we made 1 10MB file with 10 links to that file.
    
    `du -h /tmp/apaek1-workspace`

3.  101M. This is different from #2. This happened b/c we copied 10 files of size 10M, making 100M.

    `du -h /tmp/apaek1-workspace`

4. Transfering data
                      #Transfer data files using scp
                      scp apaek1@student00.cse.nd.edu:/tmp/apaek1-workspace/data'*' /tmp/apaek1-workspace/

                      #Transfer data files using sftp
                      sftp apaek1@student00.cse.nd.edu < instructions`
                      #instructions
                      get /tmp/apaek1-workspace/data* /tmp/apaek1-workspace`

                      #Transfer data files using rsync
                      rsync apaek1@student00.cse.nd.edu:/tmp/apaek1-workspace/data'*' /tmp/apaek1-workspace/
                      

5. For scp and sftp, it transfers the data every time. However, rsync only transfers data once. Rsync syncs the files, and seeing that there is no change, it wont redownload the file. This is useful b/c if youre downloading a lot of files, some of which havent been changed, it can increase speed by not downloading some files.

6. I prefer rsync because it syncs the files instead of always downloading. This can help improve speed if downloading a lot of files or large files especially during installatin of programs.


Activity 2
----------
1. Scan 'xavier h4x0r.space' for ports
    
          $nmap -Pn -p 9000-10000 xavier.h4x0r.space 
        
          Output: 
          Starting Nmap 5.51 ( http://nmap.org ) at 2016-01-29 18:54 EST
          Nmap scan report for xavier.h4x0r.space (129.74.161.24)
          Host is up (0.00045s latency).
          Not shown: 997 closed ports
          PORT      STATE    SERVICE
          9097/tcp  open     unknown
          9111/tcp  open     DragonIDSConsole
          9876/tcp  open     sd
          10000/tcp filtered snet-sensor-mgmt
          
  - There are 4 ports in the 9000-10000 range. 

2. Access the HTTP server:
    
          $ curl xavier.h4x0r.space:9876
          
          output:
          _________________________________________
          / Halt! Who goes there?                   \
          |                                         |
          | If you seek the ORACLE, you must query  |
          | the DOORMAN at /{netid}/{passcode}!     |
          |                                         |
          | To retrieve your passcode you must      |
          | decode the file at                      |
          | ~pbui/pub/oracle/${USER}/code using the |
          | BASE64 command.                         |
          |                                         |
          \ Good luck!                              /
           -----------------------------------------
                   \   ^__^
                    \  (oo)\_______
                       (__)\       )\/\
                            ||----w |
                            ||     ||

          

3. Access the code and decode it.

          $ base64 -d ~pbui/pub/oracle/apaek1/code
          
          output: 3125597eb76d392a1795b46b7a4cff7f

4. Use code and talk to the doorman
          
          $ curl xavier.h4x0r.space:9876/apaek1/3125597eb76d392a1795b46b7a4cff7f

          output: 
          / Ah yes, apaek1... I've been waiting for \
          | you.                                    |
          |                                         |
          | The ORACLE looks forward to talking to  |
          | you, but you must first retrieve a      |
          | secret message from the SLEEPER.        |
          |                                         |
          | He can be found in plain sight at       |
          | ~pbui/pub/oracle/SLEEPER... You will    |
          | need to wake him up and then signal him |
          | to HANGUP his connection. If he         |
          | recognizes you, he will give you the    |
          | message in coded form.                  |
          |                                         |
          | Once you have the message, proceed to   |
          | port 9111 and deliver the message to    |
          | the ORACLE.                             |
          |                                         |
          | Hurry! The ORACLE is wise, but she is   |
          \ not patient!                            /

4. Wake up the sleeper

          run SLEEPER by $ ~pbui/pub/oracle/SLEEPER
          find pid by $ ps -u apaek1
          find corresponding pid to SLEEPER, ie 4468
          wake up sleeper by $ kill -1 4468

          Output:  
          _________________________________________
          / Hmm... I recognize you apaek1... Here's \
          | the message you need to give to the     |
          | ORACLE:                                 |
          |                                         |
          \ bmNucngxPTE0NTQxMTMzOTk=                /
           -----------------------------------------

5. Talk to the oracle.

          telnet xavier.h4x0r.space 9111

          output:
           ________________________
          < Hello, who may you be? >
           ------------------------
          
          NAME? apaek1

           ___________________________________
           / Hmm... apaek1?                    \
           |                                   |
           | That name sounds familiar... what |
           \ message do you have for me?       /
            -----------------------------------
          
          MESSAGE? bmNucngxPTE0NTQxMTMzOTk=

           ______________________________________
           / Ah yes... apaek1!                    \
           |                                      |
           | You're smarter than I thought. I can |
           | see why the instructor likes you.    |
           |                                      |
           | You met the SLEEPER about 43 minutes |
           \ ago... What took you so long?        /
            --------------------------------------

          REASON? writing to README.md

          Congratulations apaek1! You have reached the end of this journey.

          I hope you learned something from the ORACLE :]

